package com.consolidated.oms;

import com.consolidated.oms.dao.MarketDAL;
import com.consolidated.oms.dao.USMarketDALImpl;
import com.consolidated.oms.feed.*;
import sun.plugin2.message.Message;

import javax.management.InvalidAttributeValueException;
import java.io.InputStream;
import java.nio.channels.SocketChannel;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Logger;

public final class USEquitiesConsolidatedBookImpl extends Thread implements ConsolidatedBook, Runnable {

    ConcurrentHashMap<String, MarketDAL> exchangeMap;
    ConcurrentHashMap<String, OrderBook> symbolMap;
    ConcurrentHashMap<Integer, MarketFeed> orderMap;
    private final BlockingQueue<MarketFeed> mQueue;

    /** While keepRunning is true, we keep receiving messages */
    private boolean keepRunning;

    /** isFinished is true when the thread has really exited. */
    private boolean isFinished;

    private static final Logger logger = Logger.getLogger(USEquitiesConsolidatedBookImpl.class.getName());

    public USEquitiesConsolidatedBookImpl(){
        super("USEquitiesConsolidatedBookImpl");
        exchangeMap = new ConcurrentHashMap<String, MarketDAL>();
        symbolMap = new ConcurrentHashMap<String, OrderBook>();
        orderMap = new ConcurrentHashMap<Integer, MarketFeed>();
        mQueue = new LinkedBlockingQueue<MarketFeed>();

        keepRunning = true;
        isFinished = false;

        logger.info("Consolidated Book manager started successfully");
    }

    // this method notifies thread to finish the execution
    @Override
    public void finish(){
        logger.info("Shutting down Consolidated Book Manager");
        keepRunning = false;

        for(String currExchange : exchangeMap.keySet()){
            MarketDAL currMarketDAL = exchangeMap.get(currExchange);
            currMarketDAL.disconnect();
        }

        exchangeMap.clear();
        interrupt();
    }

    public boolean isFinished(){
        return isFinished;
    }

    @Override
    public void addExchange(String exchange) {
        String exchangeLower = exchange.toLowerCase();
        if(exchangeMap.containsKey(exchangeLower)){
            logger.warning("Exchange:" + exchange + " is already added in current USEquitiesConsolidatedBookImpl object. So ignoring this.");
        }
        else{
            try {
                InputStream input = getClass().getClassLoader().getResourceAsStream("oms.properties");
                Properties prop = new Properties();
                prop.load(input);

                String ipaddress = prop.getProperty(exchangeLower + ".host");
                int port = Integer.parseInt(prop.getProperty(exchangeLower + ".port"));
                MarketDAL currExchange = new USMarketDALImpl(exchange, ipaddress, port, this);
                currExchange.connect();
                exchangeMap.put(exchange.toLowerCase(), currExchange);
            }catch(Exception ex){
                logger.warning("Exchange:" + exchange + " connection set-up failed. Error:" + ex.getMessage());
            }
        }

        return;
    }

    /**
     * This method is called when new data is received on server from channel.
     *
     * @param msg
     *            the data received
     * @param exchangeName
     *            exchange from where message came
     */
    public void processMarketFeed(String msg, String exchangeName) {
       // logger.info("Received from channel:"+channel+" "+count+" bytes");

        logger.info("queueing message:" + msg + " from Exchange:" + exchangeName);

        // assuming message is pipe delimited for different message attributes
        // and first string represents message type
        // Also split the msg object as it may contain multiple message separated by new line
        String[] msgLine = msg.split("\\r?\\n");
        for(String currMsg : msgLine) {
            if(currMsg.trim().isEmpty()){
                continue;
            }

            String[] msgArray = currMsg.split("\\|");
            String msgType = msgArray[0];
            MarketFeed feed = null;
            switch (msgType) {
                case "TOP_OF_THE_BOOK":
                    feed = new TopOfTheBook(msgArray[1], Double.parseDouble(msgArray[2]), Double.parseDouble(msgArray[3]), Double.parseDouble(msgArray[4]), Double.parseDouble(msgArray[5]));
                    //   processTopOfTheBookMsg(topFeed);
                    break;
                case "NEW_ORDER":
                    feed = new NewOrder(msgArray[1], Double.parseDouble(msgArray[2]), msgArray[3], Double.parseDouble(msgArray[4]), Integer.parseInt(msgArray[5]));
                    //   processNewOrder(newOrder);
                    break;
                case "CANCEL_ORDER":
                    feed = new CancelOrder(Integer.parseInt(msgArray[1]));
                    //    processCancelOrder(cancelOrder);
                    break;
                case "MODIFY_ORDER":
                    feed = new ModifyOrder(Integer.parseInt(msgArray[1]), Double.parseDouble(msgArray[2]));
                    //    processModifyOrder(modifyOrder);
                    break;
                default:
                    logger.warning("Incorrect message type received. Message:" + msg);
                    break;
            }

            if (feed != null) {
                try {
                    mQueue.put(feed);
                } catch (InterruptedException e) {
                    /* This is never going to happen */
                    logger.warning("Not expected error happened:" + e.getMessage());
                }
            }
        }
    }

    @Override
    public void run() {
        try {
            while (keepRunning) {
                if(mQueue.isEmpty()){
                    Thread.currentThread().sleep(1000);
                }
                MarketFeed feed = mQueue.take();

                if (feed instanceof TopOfTheBook) {
                    processTopOfTheBookMsg((TopOfTheBook) feed);
                } else if (feed instanceof OrderBasedBook) {
                    OrderBasedBook order = (OrderBasedBook) feed;
                    String orderType = order.getOrderType();

                    switch (orderType) {
                        case "NEW":
                            processNewOrder((NewOrder) order);
                            break;
                        case "CANCEL":
                            processCancelOrder((CancelOrder) order);
                            break;
                        case "MODIFY":
                            processModifyOrder((ModifyOrder) order);
                            break;
                        default:
                            logger.warning("Invalid order type found:" + orderType);
                            break;
                    }

                }
            }
        }catch (InterruptedException e) {
            logger.warning(getName()+" interrupted. Finishing network layer.");
            keepRunning = false;
        }

        isFinished = true;
    }

    @Override
    public String getTopLevel(String symbol) {
        OrderBook order = symbolMap.get(symbol);
        return order.getTop5Message();
    }

    @Override
    public void removeExchange(String exchange) {
        logger.info("Removing exchange:" + exchange + " from USEquitiesConsolidatedBookImpl object.");
        MarketDAL currExchange = exchangeMap.get(exchange.toLowerCase());
        currExchange.disconnect();
        exchangeMap.remove(exchange.toLowerCase());
    }


    private void processTopOfTheBookMsg(TopOfTheBook msg){
        String symbol = msg.getSymbol();
        if(symbolMap.containsKey(symbol)){
            OrderBook orderMsg = symbolMap.get(symbol);
            orderMsg.addBidOrder(msg.getBestBidPrice(), msg.getBestBidSize());
            orderMsg.addOfferOrder(msg.getBestOfferPrice(), msg.getBestOfferSize());
        }else {
            OrderBook newOrder = new OrderBook(symbol);
            newOrder.addBidOrder(msg.getBestBidPrice(), msg.getBestBidSize());
            newOrder.addOfferOrder(msg.getBestOfferPrice(), msg.getBestOfferSize());
            symbolMap.put(symbol, newOrder);
        }
    }

    private void processNewOrder(NewOrder order){
        String symbol = order.getSymbol();
        OrderBook orderMsg = null;
        if(symbolMap.containsKey(symbol)){
            orderMsg = symbolMap.get(symbol);
        }else {
            orderMsg = new OrderBook(symbol);
            symbolMap.put(symbol, orderMsg);
        }

        if(order.getSide() == OrderSide.BUY){
            orderMsg.addBidOrder(order.getLimitPrice(), order.getQuantity());
        }else{
            orderMsg.addOfferOrder(order.getLimitPrice(), order.getQuantity());
        }

        orderMap.put(order.getOrderId(), order);
    }

    private void processCancelOrder(CancelOrder order){
        int orderId = order.getOrderId();
        if(!orderMap.containsKey(orderId)){
            logger.warning("Order Id:" + orderId + " not found in map. Can't update anything");
            return;
        }

        MarketFeed marketOrder = orderMap.get(orderId);
        if(marketOrder instanceof NewOrder){
            NewOrder newOrder = (NewOrder)marketOrder;
            String symbol = newOrder.getSymbol();
            OrderBook orderMsg = null;
            if(symbolMap.containsKey(symbol)){
                orderMsg = symbolMap.get(symbol);
            }

            if(orderMsg == null){
                logger.warning("Order Id:" + orderId + " Symbol:" + symbol + "not found. Can't update anything");
            }else {
                if (newOrder.getSide() == OrderSide.BUY) {
                    orderMsg.removeBidOrder(newOrder.getLimitPrice(), newOrder.getQuantity());
                } else {
                    orderMsg.removeOfferOrder(newOrder.getLimitPrice(), newOrder.getQuantity());
                }

                orderMap.remove(orderId);
            }
        }

        return;
    }

    private void processModifyOrder(ModifyOrder order){
        int orderId = order.getOrderId();
        if(!orderMap.containsKey(orderId)){
            logger.warning("Order Id:" + orderId + " not found in map. Can't update anything");
            return;
        }

        MarketFeed marketOrder = orderMap.get(orderId);
        if(marketOrder instanceof NewOrder){
            NewOrder newOrder = (NewOrder)marketOrder;
            String symbol = newOrder.getSymbol();
            OrderBook orderMsg = null;
            if(symbolMap.containsKey(symbol)){
                orderMsg = symbolMap.get(symbol);
            }

            if(orderMsg == null){
                logger.warning("Order Id:" + orderId + " Symbol:" + symbol + "not found. Can't update anything");
            }else {
                double oldQty = newOrder.getQuantity();
                double newQty = order.getNewQuantity();
                if (newOrder.getSide() == OrderSide.BUY) {
                    if(oldQty < newQty){
                        orderMsg.addBidOrder(newOrder.getLimitPrice(), (newQty - oldQty));
                    }else {
                        orderMsg.removeBidOrder(newOrder.getLimitPrice(), (oldQty - newQty));
                    }
                } else {
                    if(oldQty < newQty){
                        orderMsg.addOfferOrder(newOrder.getLimitPrice(), (newQty - oldQty));
                    }else {
                        orderMsg.removeOfferOrder(newOrder.getLimitPrice(), (oldQty - newQty));
                    }
                }

                try {
                    newOrder.setQuantity(newQty);
                }catch (InvalidAttributeValueException ex){
                    logger.warning("Error in updating quantity for OrderId:" + orderId + " Error:" + ex.getMessage());
                }
            }
        }

        return;
    }
}
