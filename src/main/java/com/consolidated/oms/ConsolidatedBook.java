package com.consolidated.oms;

public interface ConsolidatedBook {
    String getTopLevel(String symbol);

    void addExchange(String exch);

    void removeExchange(String exch);

    void processMarketFeed(String msg, String exchange);

    void finish();
}
