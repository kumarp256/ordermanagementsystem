package com.consolidated.oms.dao;

public class MarketDALException extends RuntimeException {

    /**
     * Constructs an <code>MarketDALException</code> with the specified detail message.
     *
     * @param message the detail message.
     */
    public MarketDALException(String message) {

        super(message);
    }

    /**
     * Constructs an <code>MarketDALException</code> with the specified cause.
     *
     * @param cause the underlying cause.
     */
    public MarketDALException(Throwable cause) {

        super(cause);
    }

    /**
     * Constructs an <code>MarketDALException</code> with the specified detail message and cause.
     *
     * @param message the detail message.
     * @param cause the underlying cause.
     */
    public MarketDALException(String message, Throwable cause) {

        super(message, cause);
    }
}
