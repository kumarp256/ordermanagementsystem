package com.consolidated.oms.dao;

import org.springframework.util.Assert;

import java.util.logging.Logger;

public enum MarketDALFactory {
    INSTANCE;

    // flags
    private final Logger log = Logger.getLogger(MarketDALFactory.class.getName());

    /** Constructor */
    MarketDALFactory() {}

    /**
     * Create an Exchange object with default ExchangeSpecification
     *
     * <p>The factory is parameterized with the name of the exchange implementation class. This must
     * be a class extending
     *
     * @param exchangeClassName the fully-qualified class name of the exchange
     * @return a new exchange instance configured with the default
     */
    public MarketDAL createExchange(String exchangeClassName) {

        return createExchange(exchangeClassName, null, null);
    }

    /**
     * Create an Exchange object with default ExchangeSpecification
     *
     * <p>The factory is parameterized with the name of the exchange implementation class. This must
     * be a class extending
     *
     * @param exchangeClass the class of the exchange
     * @return a new exchange instance configured with the default
     */
    public <T extends MarketDAL> T createExchange(Class<T> exchangeClass) {

        return createExchange(exchangeClass, null, null);
    }

    /**
     * Create an Exchange object with default ExchangeSpecification with authentication info and API
     * keys provided through parameters
     *
     * <p>The factory is parameterized with the name of the exchange implementation class. This must
     * be a class extending
     *
     * @param exchangeClassName the fully-qualified class name of the exchange
     * @param apiKey the public API key
     * @param secretKey the secret API key
     * @return a new exchange instance configured with the default
     */
    public MarketDAL createExchange(String exchangeClassName, String apiKey, String secretKey) {

        Assert.notNull(exchangeClassName, "exchangeClassName cannot be null");

        log.info("Creating default exchange from class name");

        MarketDAL exchange = createExchangeWithoutSpecification(exchangeClassName);

        ExchangeSpecification defaultExchangeSpecification = exchange.getDefaultExchangeSpecification();
        if (apiKey != null) defaultExchangeSpecification.setApiKey(apiKey);
        if (secretKey != null) defaultExchangeSpecification.setSecretKey(secretKey);
        exchange.applySpecification(defaultExchangeSpecification);

        return exchange;
    }

    /**
     * Create an Exchange object with default ExchangeSpecification with authentication info and API
     * keys provided through parameters
     *
     * <p>The factory is parameterized with the name of the exchange implementation class. This must
     * be a class extending
     *
     * @param exchangeClass the class of the exchange
     * @param apiKey the public API key
     * @param secretKey the secret API key
     * @return a new exchange instance configured with the default
     */
    public <T extends MarketDAL> T createExchange(
            Class<T> exchangeClass, String apiKey, String secretKey) {

        Assert.notNull(exchangeClass, "exchange cannot be null");

        log.info("Creating default exchange from class name");

        T exchange = createExchangeWithoutSpecification(exchangeClass);

        ExchangeSpecification defaultExchangeSpecification = exchange.getDefaultExchangeSpecification();
        if (apiKey != null) defaultExchangeSpecification.setApiKey(apiKey);
        if (secretKey != null) defaultExchangeSpecification.setSecretKey(secretKey);
        exchange.applySpecification(defaultExchangeSpecification);

        return exchange;
    }

    /**
     * Create an Exchange object default ExchangeSpecification
     *
     * @param exchangeSpecification the exchange specification
     * @return a new exchange instance configured with the provided
     */
    public MarketDAL createExchange(ExchangeSpecification exchangeSpecification) {

        Assert.notNull(exchangeSpecification, "exchangeSpecfication cannot be null");

        log.info("Creating exchange from specification");

        String exchangeClassName = exchangeSpecification.getExchangeClassName();
        MarketDAL exchange = createExchangeWithoutSpecification(exchangeClassName);
        exchange.applySpecification(exchangeSpecification);
        return exchange;
    }

    /**
     * Create an Exchange object without default ExchangeSpecification
     *
     * <p>The factory is parameterized with the name of the exchange implementation class.
     *
     * @param exchangeClassName the fully-qualified class name of the exchange
     * @return a new exchange instance configured with the default
     */
    public MarketDAL createExchangeWithoutSpecification(String exchangeClassName) {

        Assert.notNull(exchangeClassName, "exchangeClassName cannot be null");

        log.info("Creating default exchange from class name");
        // Attempt to create an instance of the exchange provider
        try {

            // Attempt to locate the exchange provider on the classpath

            Class exchangeProviderClass = Class.forName(exchangeClassName);

            // Test that the class implements Exchange
            if (MarketDAL.class.isAssignableFrom(exchangeProviderClass)) {
                // Instantiate through the default constructor and use the default exchange specification
                return createExchangeWithoutSpecification(exchangeProviderClass);
            } else {
                throw new MarketDALException(
                        "Class '" + exchangeClassName + "' does not implement Exchange");
            }
        } catch (ClassNotFoundException e) {
            throw new MarketDALException("Problem creating Exchange (class not found)", e);
        }
    }

    /**
     * Create an Exchange object without default ExchangeSpecification
     *
     * <p>The factory is parameterized with the name of the exchange implementation class.
     *
     * @param exchangeClass the class of the exchange
     * @return a new exchange instance configured with the default
     */
    public <T extends MarketDAL> T createExchangeWithoutSpecification(Class<T> exchangeClass) {

        Assert.notNull(exchangeClass, "exchangeClassName cannot be null");

        log.info("Creating default exchange from class name");

        // Attempt to create an instance of the exchange provider
        try {

            // Instantiate through the default constructor and use the default exchange specification
            return exchangeClass.newInstance();

        } catch (InstantiationException e) {
            throw new MarketDALException("Problem creating Exchange (instantiation)", e);
        } catch (IllegalAccessException e) {
            throw new MarketDALException("Problem creating Exchange (illegal access)", e);
        }
    }
}
