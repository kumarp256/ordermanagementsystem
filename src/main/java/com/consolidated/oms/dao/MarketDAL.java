package com.consolidated.oms.dao;

import com.consolidated.oms.feed.MarketFeed;

import java.util.List;

public interface MarketDAL {
    ExchangeSpecification getExchangeSpecification();

    List<String> getExchangeSymbols();

    ExchangeSpecification getDefaultExchangeSpecification();

    void applySpecification(ExchangeSpecification exchangeSpecification);

    void onData(String msg);

    boolean connect();

    void disconnect();
}
