package com.consolidated.oms.dao;

import com.consolidated.oms.feed.MarketFeed;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public abstract class BaseMarketDAL implements MarketDAL {

    protected final Logger logger = Logger.getLogger(USMarketDALImpl.class.getName());
    protected ExchangeSpecification exchangeSpecification;
    protected List<String> symbols = new ArrayList<>();
    protected abstract void initServices();

    @Override
    public void applySpecification(ExchangeSpecification exchangeSpecification) {

        ExchangeSpecification defaultSpecification = getDefaultExchangeSpecification();

        // Check if default is for everything
        if (exchangeSpecification == null) {
            this.exchangeSpecification = defaultSpecification;
        } else {
            // Using a configured exchange
            // fill in null params with the default ones
            if (exchangeSpecification.getExchangeName() == null) {
                exchangeSpecification.setExchangeName(defaultSpecification.getExchangeName());
            }
            if (exchangeSpecification.getExchangeDescription() == null) {
                exchangeSpecification.setExchangeDescription(defaultSpecification.getExchangeDescription());
            }
            if (exchangeSpecification.getSslUri() == null) {
                exchangeSpecification.setSslUri(defaultSpecification.getSslUri());
            }
            if (exchangeSpecification.getHost() == null) {
                exchangeSpecification.setHost(defaultSpecification.getHost());
            }
            if (exchangeSpecification.getPlainTextUri() == null) {
                exchangeSpecification.setPlainTextUri(defaultSpecification.getPlainTextUri());
            }
            if (exchangeSpecification.getExchangeSpecificParameters() == null) {
                exchangeSpecification.setExchangeSpecificParameters(
                        defaultSpecification.getExchangeSpecificParameters());
            } else {
                // add default value unless it is overridden by current spec
                for (Map.Entry<String, Object> entry :
                        defaultSpecification.getExchangeSpecificParameters().entrySet()) {
                    if (exchangeSpecification.getExchangeSpecificParametersItem(entry.getKey()) == null) {
                        exchangeSpecification.setExchangeSpecificParametersItem(
                                entry.getKey(), entry.getValue());
                    }
                }
            }

            this.exchangeSpecification = exchangeSpecification;
        }


        initServices();

    }

    @Override
    public boolean connect() throws MarketDALException {

        logger.info("No remote initialization implemented for BaseMarketDAL." + exchangeSpecification.getExchangeName());
        return false;
    }

    @Override
    public List<String> getExchangeSymbols() {

        return symbols;
    }

    @Override
    public ExchangeSpecification getExchangeSpecification() {

        return exchangeSpecification;
    }


    @Override
    public String toString() {

        String name =
                exchangeSpecification != null
                        ? exchangeSpecification.getExchangeName()
                        : getClass().getName();
        return name + "#" + hashCode();
    }
}