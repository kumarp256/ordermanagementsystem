package com.consolidated.oms.dao;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.logging.Logger;

public class RecvThread extends Thread {

    private static final Logger logger = Logger.getLogger(RecvThread.class.getName());
    private SocketChannel sc = null;
    private MarketDAL marketDAL = null;

    public boolean keepRunning = true;

    /** The buffer into which we'll read data when it's available */
    private int BUFFER_SIZE = 2048;

    public RecvThread(String str, SocketChannel channel, MarketDAL dal) {
        super(str);
        this.sc = channel;
        this.marketDAL = dal;
    }

    @Override
    public void run() {

        logger.info("Inside receivemsg");
        int nBytes = 0;
        ByteBuffer buf = ByteBuffer.allocate(BUFFER_SIZE);
        try {
            while (keepRunning) {
                while ((nBytes = sc.read(buf)) > 0) {
                    buf.flip();
                    Charset charset = Charset.forName("us-ascii");
                    CharsetDecoder decoder = charset.newDecoder();
                    CharBuffer charBuffer = decoder.decode(buf);
                    String result = charBuffer.toString();

                    // Hand the data off to our market DAL implementor
                    this.marketDAL.onData(result);

                 //   buf.flip();
                    buf.clear();
                }

                Thread.currentThread().sleep(1000);
            }

        } catch (IOException e) {
            logger.warning(e.getMessage());
        } catch (InterruptedException e) {
            logger.warning(e.getMessage());
        }
    }
}
