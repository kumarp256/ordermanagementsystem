package com.consolidated.oms.dao;

import com.consolidated.oms.ConsolidatedBook;
import com.consolidated.oms.feed.MarketFeed;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class USMarketDALImpl extends BaseMarketDAL implements MarketDAL {
    String exchangeName;

    /** While keepRunning is true, we keep receiving messages */
    private boolean keepRunning;
    private boolean connected;

    /** isFinished is true when the thread has really exited */
    private boolean isFinished;

    private static final Logger logger = Logger.getLogger(USMarketDALImpl.class.getName());

    private SocketChannel socketChannel = null;
    private String ipAddress = null;
    private int port = 0;

    private ConsolidatedBook bookKeeper = null;
    public RecvThread rt = null;

    public USMarketDALImpl(String exch, String ipAddress, int port, ConsolidatedBook bookKeeper){
      //  super("USMarketDALImpl Exchange:" + exch);
        this.exchangeName = exch;
        this.ipAddress = ipAddress;
        this.port = port;
        this.bookKeeper = bookKeeper;

        this.keepRunning = true;
        this.isFinished = false;
        this.connected = false;
    }

    public SocketChannel getSocketChannel() {
        return socketChannel;
    }

    @Override
    protected void initServices() {
    }

    @Override
    public ExchangeSpecification getDefaultExchangeSpecification() {

        ExchangeSpecification exchangeSpecification =
                new ExchangeSpecification(this.getClass().getCanonicalName());
        exchangeSpecification.setSslUri(ipAddress);
        exchangeSpecification.setHost(exchangeName + ".com");
        exchangeSpecification.setPort(port);
        exchangeSpecification.setExchangeName(exchangeName);

        return exchangeSpecification;
    }

    @Override
    public boolean connect() {
        if(!connected){

            try {
                socketChannel = SocketChannel.open();
                InetSocketAddress inetSocketAddress = new InetSocketAddress(ipAddress, port);
                socketChannel.connect(inetSocketAddress);
                socketChannel.configureBlocking(false);
                logger.info(String.format("Connected to {%s}:{%d}", ipAddress, port));
                connected = true;
                receiveMessage();
            } catch (Exception e) {
                logger.severe(String.format("Failed to connect to {%s}:{%d}. Exception: {%s}", ipAddress, port, e.getMessage()));
                connected = false;
            }
        }

        return connected;
    }

    protected void receiveMessage() {
        rt = new RecvThread("Receive Thread for Exchange:" + exchangeName, socketChannel, this);
        rt.start();
    }

    public void interruptThread(){
        rt.keepRunning = false;
    }

    @Override
    public void disconnect() {
        try {
            keepRunning = false;
            socketChannel.close();
            connected = false;
        } catch (IOException e) {
            logger.severe(e.getMessage());
        }
    }
/*
    @Override
    public MarketFeed getMarketDataFeed() throws IOException {
        int numRead = -1;
        ByteBuffer readBuffer = ByteBuffer.allocate(BUFFER_SIZE);
        try {
            numRead = socketChannel.read(readBuffer);
        } catch (IOException e) {
            // The remote forcibly closed the connection, try reconnecting.
            logger.info("Remote closed connnction:" + e.getMessage());

            // retry connecting 5 times
            connected = false;
            for (int i = 0; i < 5 && !connected; ++i) {
                connected = connect();
            }

            if (!connected) {
                logger.severe("Failed to connect even after trying for 5 times to exchange:" + exchangeName);
            }

        }

        if (numRead == -1) {
            // Remote entity shut the socket down cleanly. Do the
            // same from our end and cancel the channel.
            logger.info("Remote closed connnection cleanly");
            disconnect();
            return;
        }

    }
*/
    /**
     * This method is called when new data is received on server from channel.
     *
     * @param msg
     *            the data received
     */
    public void onData(String msg) {
        logger.info("Received msg:" + msg);
        this.bookKeeper.processMarketFeed(msg, exchangeName);
    }
}
