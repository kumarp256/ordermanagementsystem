package com.consolidated.oms;

import java.util.*;

public class OrderBook {
    private String symbolName;

    // Map to store the elements
    private SortedMap<Double, Double> bidList =  new TreeMap<Double, Double>(Collections.reverseOrder());
    private SortedMap<Double, Double> offerList = new TreeMap<Double, Double>();

    public OrderBook(String symbol){
        this.symbolName = symbol;
    }

    public synchronized void addBidOrder(double bidPrice, double size){
        if(bidList.containsKey(bidPrice)){
            double oldVal = bidList.get(bidPrice);
            double newVal = bidList.get(bidPrice) + size;
            bidList.replace(bidPrice, oldVal, newVal);
        }else {
            bidList.put(bidPrice, size);
        }
    }

    public synchronized void addOfferOrder(double offerPrice, double size){
        if(offerList.containsKey(offerPrice)){
            double oldVal = offerList.get(offerPrice);
            double newVal = offerList.get(offerPrice) + size;
            offerList.replace(offerPrice, oldVal, newVal);
        }else {
            offerList.put(offerPrice, size);
        }
    }

    public synchronized void removeBidOrder(double bidPrice, double size){
        if(bidList.containsKey(bidPrice)){
            double oldVal = bidList.get(bidPrice);
            double newVal = bidList.get(bidPrice) - size;
            if(newVal == 0.0){
                bidList.remove(bidPrice);
            }else {
                bidList.replace(bidPrice, oldVal, newVal);
            }
        }
    }

    public synchronized void removeOfferOrder(double offerPrice, double size){
        if(offerList.containsKey(offerPrice)){
            double oldVal = offerList.get(offerPrice);
            double newVal = offerList.get(offerPrice) - size;
            if(newVal == 0.0){
                offerList.remove(offerPrice);
            }else {
                offerList.replace(offerPrice, oldVal, newVal);
            }
        }
    }

    public String getTop5Message(){

        Set bidSet = bidList.entrySet();
        Iterator iBid = bidSet.iterator();

        Set offerSet = offerList.entrySet();
        Iterator iOffer = offerSet.iterator();

        // Traverse map and build string
        int count = 0;
        StringBuffer sb=new StringBuffer();

        while (iBid.hasNext() && iOffer.hasNext() && (count<5)) {
            Map.Entry meBid = (Map.Entry)iBid.next();
            Map.Entry meOffer = (Map.Entry)iOffer.next();

            sb.append(String.format("Level %d: %.2f %.2f %.2f %.2f", count, meBid.getValue(), meBid.getKey(), meOffer.getKey(), meOffer.getValue()));
            sb.append( System.lineSeparator());
            ++count;
        }

        return sb.toString();
    }
}
