package com.consolidated.oms.feed;

public enum OrderSide {
    BUY,
    SELL;
}
