package com.consolidated.oms.feed;

import java.util.HashMap;

public interface MarketFeed {
    HashMap<String, Object> getFeed();
}
