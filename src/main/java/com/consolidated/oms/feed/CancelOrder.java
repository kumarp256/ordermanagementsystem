package com.consolidated.oms.feed;

import java.util.HashMap;

public class CancelOrder implements OrderBasedBook {
    String orderType = "CANCEL";
    HashMap<String, Object> message;

    int orderId;

    // ORDER_ID
    public CancelOrder(int orderId){
        message = new HashMap<String, Object>();
        this.orderId = orderId;

        message.put(OrderBasedBookAttributes.ORDER_ID.toString(), this.orderId);
    }

    @Override
    public String getOrderType() {
        return orderType;
    }

    @Override
    public HashMap<String, Object> getFeed() {
        return message;
    }

    public int getOrderId() {
        return orderId;
    }
}
