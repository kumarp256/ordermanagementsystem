package com.consolidated.oms.feed;

public enum OrderBasedBookAttributes {
    SYMBOL,
    LIMIT_PRICE,
    SIDE,
    QUANTITY,
    ORDER_ID,
    NEW_QUANTITY;
}
