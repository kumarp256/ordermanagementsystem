package com.consolidated.oms.feed;

public enum TopOfTheBookAttributes {
    SYMBOL,
    BEST_BID_PRICE,
    BEST_BID_SIZE,
    BEST_OFFER_PRICE,
    BEST_OFFER_SIZE;
}
