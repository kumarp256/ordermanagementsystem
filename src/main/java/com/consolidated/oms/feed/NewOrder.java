package com.consolidated.oms.feed;

import javax.management.InvalidAttributeValueException;
import java.util.HashMap;

public class NewOrder implements OrderBasedBook {
    String orderType = "NEW";
    HashMap<String, Object> message;

    String symbol;
    double limitPrice;
    OrderSide side;
    double quantity;
    int orderId;

    // SYMBOL, LIMIT_PRICE, SIDE (BUY/SELL), QUANTITY, ORDER_ID
    public NewOrder(String symbol, double limitPrice, String side, double qty, int orderId){
        message = new HashMap<String, Object>();
        this.symbol = symbol;
        this.limitPrice = limitPrice;
        this.side = OrderSide.valueOf(side.toUpperCase());
        this.quantity = qty;
        this.orderId = orderId;

        message.put(OrderBasedBookAttributes.SYMBOL.toString(), this.symbol);
        message.put(OrderBasedBookAttributes.LIMIT_PRICE.toString(), this.limitPrice);
        message.put(OrderBasedBookAttributes.SIDE.toString(), this.side);
        message.put(OrderBasedBookAttributes.QUANTITY.toString(), this.quantity);
        message.put(OrderBasedBookAttributes.ORDER_ID.toString(), this.orderId);
    }

    @Override
    public String getOrderType() {
        return orderType;
    }

    @Override
    public HashMap<String, Object> getFeed() {
        return message;
    }

    public String getSymbol() {
        return symbol;
    }

    public double getLimitPrice() {
        return limitPrice;
    }

    public OrderSide getSide() {
        return side;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double qty) throws InvalidAttributeValueException{
        if(qty <= 0){
            throw new InvalidAttributeValueException("Quantity can't be less than or equal to zero. Quantity sent for update:" + qty);
        }
        quantity = qty;
    }

    public int getOrderId() {
        return orderId;
    }
}
