package com.consolidated.oms.feed;

public interface OrderBasedBook extends MarketFeed {
    String getOrderType();
}
