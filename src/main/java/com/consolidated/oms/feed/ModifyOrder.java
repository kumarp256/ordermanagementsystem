package com.consolidated.oms.feed;

import java.util.HashMap;

public class ModifyOrder implements OrderBasedBook {
    String orderType = "MODIFY";
    HashMap<String, Object> message;

    int orderId;
    double newQuantity;

    // ORDER_ID, NEW_QUANTITY
    public ModifyOrder(int orderId, double qty){
        message = new HashMap<String, Object>();
        this.orderId = orderId;
        this.newQuantity = qty;

        message.put(OrderBasedBookAttributes.ORDER_ID.toString(), this.orderId);
        message.put(OrderBasedBookAttributes.NEW_QUANTITY.toString(), this.newQuantity);
    }

    @Override
    public String getOrderType() {
        return orderType;
    }

    @Override
    public HashMap<String, Object> getFeed() {
        return message;
    }

    public int getOrderId() {
        return orderId;
    }

    public double getNewQuantity() { return newQuantity; }
}
