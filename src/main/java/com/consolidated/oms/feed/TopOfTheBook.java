package com.consolidated.oms.feed;

import java.util.HashMap;

public class TopOfTheBook implements MarketFeed {
    HashMap<String, Object> message;

    String symbol;
    double bestBidPrice;
    double bestBidSize;
    double bestOfferPrice;
    double bestOfferSize;

    // SYMBOL, BEST_BID_PRICE, BEST_BID_SIZE, BEST_OFFER_PRICE, BEST_OFFER_SIZE
    public TopOfTheBook(String symbol, double bestBidPrice, double bestBidSize, double bestOfferPrice, double bestOfferSize){
        message = new HashMap<String, Object>();
        this.symbol = symbol;
        this.bestBidPrice = bestBidPrice;
        this.bestBidSize = bestBidSize;
        this.bestOfferPrice = bestOfferPrice;
        this.bestOfferSize = bestOfferSize;

        message.put(TopOfTheBookAttributes.SYMBOL.toString(), this.symbol);
        message.put(TopOfTheBookAttributes.BEST_BID_PRICE.toString(), this.bestBidPrice);
        message.put(TopOfTheBookAttributes.BEST_BID_SIZE.toString(), this.bestBidSize);
        message.put(TopOfTheBookAttributes.BEST_OFFER_PRICE.toString(), this.bestOfferPrice);
        message.put(TopOfTheBookAttributes.BEST_OFFER_SIZE.toString(), this.bestOfferSize);
    }

    @Override
    public HashMap<String, Object> getFeed() {
        return message;
    }

    public String getSymbol() {
        return symbol;
    }

    public double getBestBidPrice() {
        return bestBidPrice;
    }

    public double getBestBidSize() {
        return bestBidSize;
    }

    public double getBestOfferPrice() {
        return bestOfferPrice;
    }

    public double getBestOfferSize() {
        return bestOfferSize;
    }
}
