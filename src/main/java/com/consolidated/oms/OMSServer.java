package com.consolidated.oms;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import java.util.logging.Level;
import java.util.logging.Logger;

public final class OMSServer {
    private final static Logger logger = Logger.getLogger(OMSServer.class.getName());
    private int NUMBER_OF_THREADS = 1;
    private ConsolidatedBook consolidatedBook = null;

    public void start() {
        ExecutorService ex = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
        consolidatedBook =  new USEquitiesConsolidatedBookImpl();
        consolidatedBook.addExchange("BSE");
        ex.submit((Runnable) consolidatedBook);
    }

    public void stop() {
        if(consolidatedBook != null){
            consolidatedBook.removeExchange("BSE");
            consolidatedBook.finish();
        }
    }

    public static void main(String[] args) throws Exception {
        try {
            final OMSServer executor = new OMSServer();
            executor.start();
            logger.info("press <enter> to quit");
            System.out.println("press <enter> to quit");
            System.in.read();
            executor.stop();
        } catch (final Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
