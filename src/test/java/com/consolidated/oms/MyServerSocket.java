package com.consolidated.oms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServerSocket implements Runnable {

    private ServerSocket server;
    private Socket client;
    private int port;
    public MyServerSocket(String ipAddress, int port) throws Exception {
        if (ipAddress != null && !ipAddress.isEmpty())
            this.server = new ServerSocket(port, 1, InetAddress.getByName(ipAddress));
        else
            this.server = new ServerSocket(port, 1, InetAddress.getLocalHost());
    }

    public InetAddress getSocketAddress() {
        return this.server.getInetAddress();
    }

    public int getPort() {
        return this.server.getLocalPort();
    }

    @Override
    public void run() {
        String data = null;
        try {
            client = this.server.accept();
            String clientAddress = client.getInetAddress().getHostAddress();
            System.out.println("\r\nNew connection from " + clientAddress);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(client.getInputStream()));
            while ((data = in.readLine()) != null) {
                System.out.println("\r\nMessage from " + clientAddress + ": " + data);
            }
        } catch(Exception ex){
            System.out.println("Excpetion in MyServerSocket:" + ex.getMessage());
        }

    }

    public boolean sendMessage(String msg){
        boolean sent = false;
        try {
            OutputStreamWriter osw = new OutputStreamWriter(client.getOutputStream());
            BufferedWriter bw = new BufferedWriter(osw);
            bw.write(msg);
            bw.flush();
            sent = true;
        }catch (Exception ex){
            System.out.println("Excpetion in sending msg:" + msg + " Error:" + ex.getMessage());
        }

        return sent;
    }

    public void close(){
        try {
            client.close();
            server.close();
        }catch(Exception ex){
            System.out.println("Excpetion in closing Socket. Error:" + ex.getMessage());
        }
    }
}
