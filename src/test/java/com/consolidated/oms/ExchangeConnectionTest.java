package com.consolidated.oms;

import com.consolidated.oms.dao.USMarketDALImpl;
import org.junit.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExchangeConnectionTest {
    private static String ipaddress;
    private static int port;
    private static MyServerSocket app = null;

    @BeforeClass
    public static void setUpClass(){

        try{
            InputStream input = ExchangeConnectionTest.class.getClassLoader().getResourceAsStream("oms.properties");

            Properties prop = new Properties();
            prop.load(input);

            ipaddress = prop.getProperty("bse.host");
            port = Integer.parseInt(prop.getProperty("bse.port"));

            app = new MyServerSocket(ipaddress, port);
            Thread serverThread = new Thread(app);
            serverThread.setName("Server Test");
            serverThread.start();
        }catch(Exception ex){
            System.out.println("Exception connecting to local ipaddress" + ex.getMessage());
            Assert.fail(ex.getMessage());
        }
    }

    @AfterClass
    public static void tearDownClass(){
        app.close();
    }

    // @Test
    public void testConnection(){

        USMarketDALImpl marketDAL = new USMarketDALImpl("BSE", ipaddress, port, null);

        boolean connected = marketDAL.connect();

        Assert.assertTrue(connected);

        marketDAL.disconnect();
    }

    public boolean sendMsg(String msg) {
        return app.sendMessage(msg);
    }

    @Test
    public void testReceiveMsg(){
        ExecutorService ex = Executors.newFixedThreadPool(1);
        ConsolidatedBook consolidatedBook =  new USEquitiesConsolidatedBookImpl();
        consolidatedBook.addExchange("BSE");
        ex.submit((Runnable) consolidatedBook);

        // SYMBOL, BEST_BID_PRICE, BEST_BID_SIZE, BEST_OFFER_PRICE, BEST_OFFER_SIZE
        String msg = "TOP_OF_THE_BOOK|INFY|100.50|200|98.25|100\n";
        boolean sent = sendMsg(msg);
        Assert.assertTrue(sent);

        msg = "TOP_OF_THE_BOOK|INFY|103.50|100|99.25|150\n";
        sent = sendMsg(msg);
        Assert.assertTrue(sent);

        msg = "TOP_OF_THE_BOOK|INFY|99.50|150|98.00|200\n";
        sent = sendMsg(msg);
        Assert.assertTrue(sent);

        msg = "TOP_OF_THE_BOOK|INFY|102.10|300|100.00|100\n";
        sent = sendMsg(msg);
        Assert.assertTrue(sent);

        msg = "TOP_OF_THE_BOOK|INFY|101.60|150|98.75|50\n";
        sent = sendMsg(msg);
        Assert.assertTrue(sent);

        try {
            Thread.currentThread().sleep(10000);
        }catch (Exception e){
            System.out.println("Error:" + e.getMessage());
        }

        String topMsg = consolidatedBook.getTopLevel("INFY");

        String compareMsg="Level 0: 100.00 103.50 98.00 200.00\r\n" +
                "Level 1: 300.00 102.10 98.25 100.00\r\n" +
                "Level 2: 150.00 101.60 98.75 50.00\r\n" +
                "Level 3: 200.00 100.50 99.25 150.00\r\n" +
                "Level 4: 150.00 99.50 100.00 100.00\r\n";
        boolean isSame = topMsg.equals(compareMsg);
        Assert.assertTrue(isSame);

        msg = "NEW_ORDER|INFY|102|Buy|150|12345\n";
        sent = sendMsg(msg);
        Assert.assertTrue(sent);

        try {
            Thread.currentThread().sleep(5000);
        }catch (Exception e){
            System.out.println("Error:" + e.getMessage());
        }

        topMsg = consolidatedBook.getTopLevel("INFY");

        compareMsg="Level 0: 100.00 103.50 98.00 200.00\r\n" +
                "Level 1: 300.00 102.10 98.25 100.00\r\n" +
                "Level 2: 150.00 102.00 98.75 50.00\r\n"  +
                "Level 3: 150.00 101.60 99.25 150.00\r\n" +
                "Level 4: 200.00 100.50 100.00 100.00\r\n";

        isSame = topMsg.equals(compareMsg);
        Assert.assertTrue(isSame);

        msg = "NEW_ORDER|INFY|100|Sell|50|12346\n";
        sent = sendMsg(msg);
        Assert.assertTrue(sent);

        try {
            Thread.currentThread().sleep(5000);
        }catch (Exception e){
            System.out.println("Error:" + e.getMessage());
        }

        topMsg = consolidatedBook.getTopLevel("INFY");

        compareMsg="Level 0: 100.00 103.50 98.00 200.00\r\n" +
                "Level 1: 300.00 102.10 98.25 100.00\r\n" +
                "Level 2: 150.00 102.00 98.75 50.00\r\n"  +
                "Level 3: 150.00 101.60 99.25 150.00\r\n" +
                "Level 4: 200.00 100.50 100.00 150.00\r\n";

        isSame = topMsg.equals(compareMsg);
        Assert.assertTrue(isSame);

        msg = "CANCEL_ORDER|12346\n";
        sent = sendMsg(msg);
        Assert.assertTrue(sent);

        try {
            Thread.currentThread().sleep(5000);
        }catch (Exception e){
            System.out.println("Error:" + e.getMessage());
        }

        topMsg = consolidatedBook.getTopLevel("INFY");

        compareMsg="Level 0: 100.00 103.50 98.00 200.00\r\n" +
                "Level 1: 300.00 102.10 98.25 100.00\r\n" +
                "Level 2: 150.00 102.00 98.75 50.00\r\n"  +
                "Level 3: 150.00 101.60 99.25 150.00\r\n" +
                "Level 4: 200.00 100.50 100.00 100.00\r\n";

        isSame = topMsg.equals(compareMsg);
        Assert.assertTrue(isSame);

        msg = "MODIFY_ORDER|12345|200\n";
        sent = sendMsg(msg);
        Assert.assertTrue(sent);

        try {
            Thread.currentThread().sleep(5000);
        }catch (Exception e){
            System.out.println("Error:" + e.getMessage());
        }

        topMsg = consolidatedBook.getTopLevel("INFY");

        compareMsg="Level 0: 100.00 103.50 98.00 200.00\r\n" +
                "Level 1: 300.00 102.10 98.25 100.00\r\n" +
                "Level 2: 200.00 102.00 98.75 50.00\r\n"  +
                "Level 3: 150.00 101.60 99.25 150.00\r\n" +
                "Level 4: 200.00 100.50 100.00 100.00\r\n";

        isSame = topMsg.equals(compareMsg);
        Assert.assertTrue(isSame);
    }
}
